///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 05a - Animal Farm 2
///
/// @file nunu.cpp
/// @version 1.0
///
/// Exports data about all nunu fish
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 05a - AnimalFarm2 - EE 205 - Spr 2021
/// @date   10_FEB_2021
///////////////////////////////////////////////////////////////////////////////


#include <string>
#include <iostream>

#include "nunu.hpp"

using namespace std;

namespace animalfarm {
	
   Nunu::Nunu( bool newNative, enum Color newColor, enum Gender newGender, float newFavoriteTemp ) {
	   gender         = newGender;
	   species        = "Fistularia chinensis";
	   scaleColor     = newColor;
      isNative       = newNative;
      favoriteTemp   = newFavoriteTemp;
   }

   void Nunu::printInfo() {
	   cout << "Nunu"                                                    << endl;
      cout << "   Is native = [" << std::boolalpha << isNative << "]"   << endl;
	   Fish::printInfo();
   }
}
