///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4 
///
/// @file list.hpp
/// @version 1.0
///
/// A custom implementation of a single linked list
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   28_MAR_2021
///////////////////////////////////////////////////////////////////////////////
#pragma once
#include <iostream>
#include "node.hpp"

class SingleLinkedList {
   protected:
      Node* head = nullptr;
   public:
      const bool empty()                     const;
      void push_front(Node* newNode);
      Node* pop_front();
      Node* get_first()                      const;
      Node* get_next(const Node* currentNode) const;
      unsigned int size()                    const;
      void swap(Node*);
      void bubbleSort();
};
