///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file animalfactory.cpp
/// @version 1.0
///
/// Randomly creates new animals
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   28_MAR_2021
///////////////////////////////////////////////////////////////////////////////
#include "animalfactory.hpp"

namespace animalfarm {
   Animal* AnimalFactory::getRandomAnimal() {
      int typeOfAnimal = rand() % 6;
      switch(typeOfAnimal) {
         case 0: return new Cat(generateRandomName(), generateRandomColor(), generateRandomGender());
         case 1: return new Dog(generateRandomName(), generateRandomColor(), generateRandomGender());
         case 2: return new Nunu(generateRandomBool(), RED, generateRandomGender(), generateRandomTemp());
         case 3: return new Aku(generateRandomWeight(), SILVER, generateRandomGender(), generateRandomTemp());
         case 4: return new Palila(generateRandomLocation(), YELLOW, generateRandomGender(), generateRandomBool());
         case 5: return new Nene(generateRandomName(), BROWN, generateRandomGender(), generateRandomBool());
         default: return 0;
      }
   }
}
