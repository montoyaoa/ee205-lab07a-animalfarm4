///////////////////////////////////////////////////////////////////////////////
/// University of Hawaii, College of Engineering
/// EE 205  - Object Oriented Programming
/// Lab 07a - Animal Farm 4
///
/// @file animal.hpp
/// @version 1.0
///
/// Exports data about all animals
///
/// @author Osiel Montoya <montoyao@hawaii.edu>
/// @brief  Lab 07a - AnimalFarm4 - EE 205 - Spr 2021
/// @date   10_FEB_2021
///////////////////////////////////////////////////////////////////////////////

#pragma once

#include <string>
#include "node.hpp"

using namespace std;

namespace animalfarm {

   enum Gender { MALE, FEMALE, UNKNOWN };
   enum Color { BLACK, WHITE, RED, SILVER, YELLOW, BROWN};

   //note: Animal now inherits from Node, allowing it to be linked into a list
   class Animal : public Node {
   public:
	   Animal();
      ~Animal();

      enum Gender gender;
	   string      species;

	   virtual const string speak() = 0;
	   virtual void printInfo();
	   string colorName  (enum Color color);
	   string genderName (enum Gender gender);

      virtual bool operator>(const Node& r);

      static bool compareBySpecies(const Animal* node1, const Animal* node2);
   };

}
